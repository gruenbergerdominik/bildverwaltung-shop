<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    session_start();

    include "partials/head.php";
    ?>
</head>

<body>
    <?php 
	
        if (isset($_GET["page"]))
        {
                $page = $_GET["page"];
        }
        else
	{
            $page = 'partials/gallery.php';
        }
        
        include "partials/scripts.php";

        include "partials/nav.php";
        
        include "partials/message.php";
        
        include $page;    
            
        include "partials/forms.php";
        
        include "partials/footer.php";
        
        if(isset($_SESSION["message"]))
        {
            unset($_SESSION["message"]);
        }
        if(isset($_SESSION["message-type"]))
        {
            unset($_SESSION["message-type"]);
        }

        include "dropzone.php";
    ?>
	<script src="assets/js/slide.js"></script>
    <script src="assets/js/selectize.js"></script>

    <script>
        $('.uploader').click(function() {
            $('#upload').click();
        })

        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#uploadPreview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload").on('change', function() {
            readURL(this);
        });
    </script>
    <?php
    if(isset($_SESSION["message"]))
    {
    unset($_SESSION["message"]);
    }
    if(isset($_SESSION["message-type"]))
    {
    unset($_SESSION["message-type"]);
    }
    ?>
</body>

</html>