<?php
require_once("model/User.class.php");

session_start();
    
    if (!isset($db))
    {
        $db = new DB();
    }
    
    $resultUser = $db->getCurUserdata($_SESSION["userid"]);
    $curUser = $resultUser->fetch_assoc();
    
    $errors = array();
	
    
    if(!empty($_POST["password"]))
    {
        $password = $_POST["password"];
        
        if(password_verify($password, $curUser["passwort"]))
        {
            if(!empty($_POST["passwordNew"]))
            {
                $passwordNew = $_POST["passwordNew"];
                if(!empty($_POST["passwordConfirmation"]))
                {
                    $passwordConfirmation = $_POST["passwordConfirmation"];
                }
                $errorsPw = array();

                $password_hash = password_hash($passwordNew, PASSWORD_DEFAULT);

                if(strlen($passwordNew) > 60)
                {
                    array_push($errorsPw, "Passwort zu lang! (maximal 60 Zeichen)");
                }
                if($passwordNew != $passwordConfirmation)
                {
                    array_push($errorsPw, "Die beiden Passwörter stimmen nicht überein!");
                }

                if(password_verify($passwordNew, $curUser["password"]))
                {
                    array_push($errorsPw, "Passwort unverändert!");
                }

                if(count($errorsPw) === 0)
                {
                    $db->updatePassword($password_hash, $_SESSION["userid"]);
                }
                else
                {
                    $errors = array_merge($errors, $errorsPw);
                }
            }
            
        }
        else
        {
             array_push($errors, "Falsches Passwort!");
        }
    }
    else
    {
        array_push($errors, "Kein Passwort eingegeben!");
    }
    
    unset($db);

    if(count($errors) > 0)
    {
        $_SESSION["message"] = $errors;
        $_SESSION["message-type"] = "alert-danger";
    }
    
    header("Location: index.php");
?>