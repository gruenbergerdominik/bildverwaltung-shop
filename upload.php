<?php


$uploadFolder = 'pictures' . DIRECTORY_SEPARATOR . 'full';
$tumbnailFolder = 'pictures' . DIRECTORY_SEPARATOR . 'thumbnail';

if (!empty($_FILES)) 
{
    $originalTempFile = $_FILES['file']['tmp_name'];             
    
    $time = number_format(microtime(true),4,"","");

    $originalFile =  dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR . $time . "║" . $_FILES['file']['name'];
    $thumbnailFile = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $tumbnailFolder . DIRECTORY_SEPARATOR . $time . "║" . $_FILES['file']['name'];
    move_uploaded_file($originalTempFile, $originalFile);

    createThumbnail($originalFile, $thumbnailFile, 200);
    echo "File uploaded!";
} 
else 
{
    ?> Kein File wurde hochgeladen! <?php
} 


function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) 
{
    $type = exif_imagetype($src);

    if (!$type || ($type != IMAGETYPE_JPEG && $type != IMAGETYPE_PNG)) 
    {
        return null;
    }

    if ($type === IMAGETYPE_JPEG)
    {
        $image = imagecreatefromjpeg($src);
    }
    else if ($type === IMAGETYPE_PNG)
    {
        $image = imagecreatefrompng($src);
    }

    if (!$image) 
    {
        return null;
    }

    $width = imagesx($image);
    $height = imagesy($image);

    if ($targetHeight == null) 
    {
        $ratio = $width / $height;

        if ($width > $height) 
        {
            $targetHeight = floor($targetWidth / $ratio);
        }
        else 
        {
            $targetHeight = $targetWidth;
            $targetWidth = floor($targetWidth * $ratio);
        }
    }

    $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

    if ($type === IMAGETYPE_PNG) 
    {
        imagecolortransparent($thumbnail, imagecolorallocate($thumbnail, 0, 0, 0));

        imagealphablending($thumbnail, false);
        imagesavealpha($thumbnail, true);
    }

    imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    
    if ($type === IMAGETYPE_JPEG)
    {
        return imagejpeg($thumbnail, $dest, 100);
    }
    else if ($type === IMAGETYPE_PNG) 
    {
        return imagepng($thumbnail, $dest, 0);
    }

}

?> 
