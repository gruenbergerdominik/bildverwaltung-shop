<?php
require_once("model/User.class.php");

session_start();
    
    if (!isset($db))
    {
        $db = new DB();
    }
    
    $resultUser = $db->getCurUserdata($_SESSION["userid"]); //getting current user data
    $curUser = $resultUser->fetch_assoc();
    
    $errors = array();
	
    
    if(!empty($_POST["password"])) //error checking
    {
        $password = $_POST["password"];
        
        if(password_verify($password, $curUser["passwort"])) //checking correct password
        {
                
            if(!empty($_POST["email"]))
            {
                $email = $_POST["email"];
                $errorsEmail = array();

                if (strlen($email) > 64)
                {
                    array_push($errorsEmail, "Email zu lang! (maximal 64 Zeichen)");
                }
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                {
                    array_push($errorsEmail, "Falsches email Format!");
                }
                if ($email === $curUser["email"])
                {
                    array_push($errorsEmail, "Email unverändert!");
                }
                else 
                {
                    $resultSetEmail = $db->checkDbForEmail($email);
                    if($resultSetEmail[0])
                    {
                        array_push($errorsEmails, "Email existiert bereits!");
                    }
                }

                if(count($errorsEmail) === 0) //update email
                {
                    $db->updateEmail($email, $_SESSION["userid"]);
                }   
                else
                {
                    $errors = array_merge($errors, $errorsEmail);
                }
            }

            if(!empty($_POST["firstname"])) //checking for errors with firstname
            {
                $firstname = $_POST["firstname"];
                $errorsFirst = array();  

                if(strlen($firstname) > 32)
                {
                    array_push($errorsFirst, "Vorname zu lang! (maximal 32 Zeichen)");
                }
                if($firstname === $curUser["vorname"])
                {
                    array_push($errorsFirst, "Vorname unverändert!");
                }

                if(count($errorsFirst) === 0)
                {
                    $db->updateFirstname($firstname, $_SESSION["userid"]); //update firstname
                }
                else
                {
                    $errors = array_merge($errors, $errorsFirst);
                }
            }


            if(!empty($_POST["lastname"])) //checking for errors with lastname
            {
                $lastname = $_POST["lastname"];
                $errorsLast = array();

                if(strlen($lastname) > 32)
                {
                    array_push($errorsLast, "Nachname zu lang! (maximal 32 Zeichen)");
                }
                if($lastname === $curUser["nachname"])
                {
                    array_push($errorsLast, "Nachname unverändert!");
                }

                if(count($errorsLast) === 0)
                {
                    $db->updateLastname($lastname, $_SESSION["userid"]); //updating lastname
                }
                else
                {
                    $errors = array_merge($errors, $errorsLast);
                }
            }
            
            if (empty($_POST["email"]) && empty($_POST["firstname"]) && empty($_POST["lastname"])) //empty array error
            {
                array_push($errors, "Keine Daten verändert!");
            }
        }
        else
        {
             array_push($errors, "Falsches Passwort!");
        }
    }
    else
    {
        array_push($errors, "Kein Passwort eingegeben!");
    }
    
    /*if(!empty($_POST["password"]))
    {
        $password = $_POST["password"];
        $passwordConfirmation = $_POST["passwordConfirmation"];
        $errorsPw = array();
        
        $password_hash = password_hash($password, PASSWORD_DEFAULT);
        
        if(strlen($password) > 60)
        {
            array_push($errorsPw, "Passwort zu lang! (maximal 60 Zeichen)");
        }
        if($password != $passwordConfirmation)
        {
            array_push($errorsPw, "Die beiden Passwörter stimmen nicht überein!");
        }
        
        if(password_verify($password, $curUser["password"]))
        {
            array_push($errorsPw, "Passwort unverändert!");
        }
        
        if(count($errorsPw) === 0)
        {
            $db->updatePassword($password_hash, $_SESSION["userid"]);
        }
        else
        {
            $errors = array_merge($errors, $errorsPw);
        }
    }*/
    
    unset($db);

    if(count($errors) > 0)
    {
        $_SESSION["message"] = $errors;
        $_SESSION["message-type"] = "alert-danger";
    }
    
    header("Location: index.php");
?>