let value_delimiter = "╓";

let filter_options = [{"id":"filetype╓png","class":"filetype","title":"File extension *.PNG"},
    {"id":"filetype╓jpg","class":"filetype","title":"File extension *.JPG"},
    {"id":"size╓1000","class":"size","title":"Larger then 1KB"},
    {"id":"size╓100000","class":"size","title":"Larger then 100KB"},
    {"id":"size╓5000000","class":"size","title":"Larger then 5MB"}];

let filter_groups = [{"value":"filetype","label":"File extension","label_hint":"Shows only the files with the selected file extension"},
    {"value":"size","label":"File Size","label_hint":"Shows file that have a size larger than selected!"}];

var filter = $('#filter').selectize(
    {
    persist: false,
    plugins: ['remove_button'],
    createOnBlur: true,
    delimiter: "║",
    create: function (input, callback)      
    {
        callback(
        {           
            title: "contains \"" + input + "\"",
            id: "fulltext" + value_delimiter + input
        });
    },
    valueField: 'id',
    labelField: 'title',
    onItemAdd: function() 
    {
        $.ajax(
        {
            method: "POST",
            url: "imagesNew.php",
            data: { filterParams: this.getValue() }
        })
        .done(function( res ) 
        {
            $('#image-list').empty();
            setImages(res);
        });
    },
    onItemRemove: function() 
    {
        $.ajax(
        {
            method: "POST",
            url: "imagesNew.php",
            data: { filterParams: this.getValue() }
        })
        .done(function( res ) 
        {
            $('#image-list').empty();
            setImages(res);
        });
    },
    placeholder: "Filter ...",
    searchField: 'title',
    options: filter_options,
    optgroups: filter_groups,
    optgroupField: 'class',
    render: 
    {
        item: function(item, escape) 
        {
            return '<div class="item ' + escape(item.class) + '" data-value="' + escape(item.id) + '">' + escape(item.title) + '</div>';
        },
        optgroup_header: function(data, escape) 
        {
            return '<div class="optgroup-header ' + escape(data.value) + '">' + escape(data.label) + ' <span class="optgroup-header-hint ' + escape(data.value) + '">' + escape(data.label_hint) + '</span></div>';
        }
    }
    })[0].selectize;

    $.ajax(
    {
        method: "POST",
        url: "imagesNew.php",
        data: { filterParams: $('#filter').val()}
    })
    .done(function( res ) 
    {
        $('#image-list').empty();
        setImages(res);
    });