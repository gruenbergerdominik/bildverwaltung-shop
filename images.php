<?PHP

$uploadFolder = 'pictures' . DIRECTORY_SEPARATOR . 'full';
$thumbnailFolder = 'pictures' . DIRECTORY_SEPARATOR . 'thumbnail';

$original_files = array_diff(scandir(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder), array('.', '..'));
$thumbnails_files = array_diff(scandir(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder), array('.', '..'));

$filterparams = null;

$data = array();

if (!empty($_POST["filterParams"]))
{
    $filterparams = explode("║",$_POST["filterParams"]);
}

$image_count = 0;

foreach($original_files as $file)
{
    $image_count += 1;
}

if(!empty($filterparams))
{
    foreach($filterparams as $filter)
    {
        $temp = array();
    
        $f = explode("╓", $filter)[0];
        $v = explode("╓", $filter)[1];
    
        if($f == "filetype")
        {
            if($v == "png")
            {
                foreach($original_files as $file)
                {
                    if(exif_imagetype(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file) == IMAGETYPE_PNG)
                    {
                        array_push($temp, $file);
                    }
                }
            } 
            else if($v == "jpg")
            {
                foreach($original_files as $file)
                {
                    if(exif_imagetype(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file) == IMAGETYPE_JPEG)
                    {
                        array_push($temp, $file);
                    }
                }
            }
        } 
        else if($f == "fulltext")
        {
            foreach($original_files as $file)
            {
                if(strpos(strtolower($file), strtolower($v)) !== false)
                {
                    array_push($temp, $file);
                }
            }
        } 
        elseif($f == "size")
        {
            if($v == "1000")
            {
                foreach($original_files as $file)
                {
                    if(filesize(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file) >= 1000)
                    {
                        array_push($temp, $file);
                    }
                }
            } 
            else if($v == "100000")
            {
                foreach($original_files as $file)
                {
                    if(filesize(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file) >= 100000)
                    {
                        array_push($temp, $file);
                    }
                }
            } 
            else if($v == "5000000")
            {
                foreach($original_files as $file)
                {
                    if(filesize(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file) >= 5000000)
                    {
                        array_push($temp, $file);
                    }
                }
            }
        }


        $original_files = $temp;
    }

    foreach($original_files as $file)
    {
        array_push($data, array("originalFile" => "pictures/full/" . $file,"thumbnailFile" => "pictures/thumbnail/" . $file, "name" => $file, "fileSize" => filesize(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file)));
    }
} 
else 
{
    foreach($original_files as $file)
    {
        array_push($data, array("originalFile" => "pictures/full/" . $file,"thumbnailFile" => "pictures/thumbnail/" . $file, "name" => $file, "fileSize" => filesize(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR .$file)));
    }
}

header('Content-type:application/json;charset=utf-8');

echo json_encode(array("images" => $data, "allImageCount" => $image_count));