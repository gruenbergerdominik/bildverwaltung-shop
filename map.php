<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
        session_start();
    
        include "partials/head.php";
    ?>
</head>
<body>

    <?php 
        include "partials/nav.php";
        
        include "partials/message.php";
        
        include "partials/scripts.php";

        include "partials/map.php";
        
        if(isset($_SESSION["message"]))
        {
            unset($_SESSION["message"]);
        }
        if(isset($_SESSION["message-type"]))
        {
            unset($_SESSION["message-type"]);
        }
    ?>
</body>
</html>