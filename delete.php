<?php

    $uploadFolder = 'pictures' . DIRECTORY_SEPARATOR . 'full';
    $thumbnailFolder = 'pictures' . DIRECTORY_SEPARATOR . 'thumbnail';

    if (!empty($_POST["file"]))
    {
        $file = $_POST["file"];
    }
    
    unlink(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR . $file);
    unlink(dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $thumbnailFolder . DIRECTORY_SEPARATOR . $file);
    
    include "images.php";

?>
