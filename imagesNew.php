<?php
    require_once("model/Image.class.php");

    $imagedb = new ImageDB(dirname( __FILE__ ));

    header('Content-type:application/json;charset=utf-8');

    $filterparams = null;

    if (!empty($_POST["filterParams"]))
    {
        $filterparams = explode("║",$_POST["filterParams"]);
    }

    $tempQuery = array();

    if(!empty($filterparams)){   
        foreach($filterparams as $filter) {
            $f = explode("╓", $filter)[0];
            $v = explode("╓", $filter)[1];
            if($f == "filetype") {
                if($v == "png") {
                    array_push($tempQuery, " `image_type` LIKE '.png' ");
                } else if($v == "jpg") {
                    array_push($tempQuery, " `image_type` LIKE '.jpeg' ");
                }
            } else if($f == "fulltext") { 
                array_push($tempQuery, " `title` LIKE '%" . $v . "%' ");
            } elseif($f == "size") {
                if($v == "5000000")
                {
                    array_push($tempQuery, " `imagesize` >= " . $v . " ");
                }
                else if($v == "100000")
                {
                    array_push($tempQuery, " `imagesize` >= " . $v . " ");
                }
                else if($v == "1000")
                {
                    array_push($tempQuery, " `imagesize` >= " . $v . " ");
                }
            }    
        }    
    }

    $imagesRaw = $imagedb->getImages($tempQuery);

    $images = array();

    foreach ($imagesRaw as &$image) {
        $img = array();
        $img["name"] = $image[2];
        $img["originalFile"] = "pictures/full/" . $image[0] . $image[4];
        $img["thumbnailFile"] = "pictures/thumbnail/" . $image[0] . $image[4];
        if ($image[7] === NULL && $image[8] === NULL)
        {
            $image[7] = "";
            $image[8] = "";
        }
        $img["latitude"] = $image[7];
        $img["longitude"] = $image[8];
        $img["user"] = $image[12];
        array_push($images, $img);
    }

    $query = "SELECT * FROM `images`";

    if(count($tempQuery) > 0) {
        $query = $query . " WHERE " . implode(" AND ", $tempQuery) . ";";
    } else {
        $query = $query . ";";
    }

    echo json_encode( array("images" => $images, "query" => $query,"allImageCount" => count($images)));
