<?php

class DB {
    private $username = "root";
    private $host = "localhost";
    private $password = "";
    private $db = "images";
    
    private $connection = NULL;
    
    function __construct() //construct
    { 
        $this->connection = $this->connectToDb();
    }
    
    function __destruct() //destruct
    {
        $this->closeConnection();
    }
    
    private function connectToDb() //open connection to db
    {
        $connection = new mysqli($this->host, $this->username, $this->password, $this->db);
            
        if ($connection->connect_error) 
        {
            die('Connect Error (' . $connection->connect_errno . ') '. $connection->connect_error);
        }
        
        return $connection;
    }
    
    private function closeConnection() //close connection to db
    {
        $this->connection->close();
    }
    
    public function realEscapeString($string) //sql safety measures
    {
        return $this->connection->real_escape_string($string);
    }
    
    public function registerUser($uid, $username, $password, $firstname, $lastname, $email) //registration
    {             
        if($this->connection)
        {
            $query = "INSERT INTO `user` (`id`, `username`, `passwort`, `vorname`, `nachname`, `email`, `is_admin`) VALUES (?, ?, ?, ?, ?, ?, 0);";
            $statement = $this->connection->prepare($query);
            if(!$statement){
                die($this->connection->error);
            }
            $statement->bind_param("ssssss", $uid, $username, $password, $firstname, $lastname, $email);
            $statement->execute();
            return true;
        }
        return false;
    }
    
    public function checkDbForEmail($email) //search DB for certain mail
    {
        if($this->connection)
        {
            $statement = $this->connection->prepare("SELECT COUNT(*) FROM user WHERE email=?");
            $statement->bind_param("s", $email);
            $statement->execute();
        }
        return $statement->get_result()->fetch_array();
    }
    
    public function checkDbForUsername($username) //search DB for certain username
    {
        if($this->connection)
        {
            $statement = $this->connection->prepare("SELECT COUNT(*) FROM user WHERE username=?");
            $statement->bind_param("s", $username);
            $statement->execute();
        }
        return $statement->get_result()->fetch_array();
    }
    
    public function checkDbForUid($uid) //search DB for certain uid
    {
        if($this->connection)
        {
            $statement = $this->connection->prepare("SELECT * FROM `user` WHERE `id` = ?;");
            $statement->bind_param("s", $uid);
            $statement->execute();
        }  
        return $statement->get_result();
    }
    
    public function getLoginUserdata($username) ////get information from user
    {
        if($this->connection)
        {
           $statement = $this->connection->prepare("Select username, passwort, id, is_admin from user where username=?");
           $statement->bind_param("s", $username);
           $statement->execute();
        }  
        return $statement->get_result();
    }
    
    public function getCurUserdata($uid) //get userinformation
    {
        if($this->connection)
        {
           $statement = $this->connection->prepare("Select vorname, nachname, email, passwort from user where id=?");
           $statement->bind_param("s", $uid);
           $statement->execute();
        }  
        return $statement->get_result();
    }
    
    public function updateEmail($email, $userid) //update email of user
    {
        $statement = $this->connection->prepare("update user set email=? where id=?");
        $statement->bind_param("ss", $email, $userid);
        $statement->execute();
    }
    
    public function updateFirstname($firstname, $userid) //update firstname of user
    {
        $statement = $this->connection->prepare("update user set vorname=? where id=?");
        $statement->bind_param("ss", $firstname, $userid);
        $statement->execute();
    }
    
    public function updateLastname($lastname, $userid) //update lastname of user
    {
        $statement = $this->connection->prepare("update user set nachname=? where id=?");
        $statement->bind_param("ss", $lastname, $userid);
        $statement->execute();
    }
    
    public function updatePassword($password, $userid) //update password
    {
        $statement = $this->connection->prepare("update user set passwort=? where id=?");
        $statement->bind_param("ss", $password, $userid);
        $statement->execute();
    }
}
?>