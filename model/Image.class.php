<?php
class ImageDB {
    private $username = "root";
    private $host = "localhost";
    private $password = "";
    private $db = "images";
    public $dir = "";


    private $connection = NULL;
    
    function __construct($dir) //construct
    { 	
        $this->dir = $dir;
        $this->connection = $this->connectToDb();
    }
    
    function __destruct() //destruct
    {
        $this->closeConnection();
    }
    
    private function connectToDb() //open connection
    {
        $connection = new mysqli($this->host, $this->username, $this->password, $this->db);
            
        if ($connection->connect_error) 
        {
            die('Connect Error (' . $connection->connect_errno . ') '. $connection->connect_error);
        }
        
        return $connection;
    }
    
    public function uploadImage($file, $title, $description, $tags, $user) { //upload image
        if($this->connection)
        {
            $uuid = $this->createUid(); //create UID

            $original_file = $file["tmp_name"]; //create/load variables
            $original_filename = $file["name"];
            $type = image_type_to_extension(exif_imagetype($original_file));   
            $size = filesize($original_file);
            
            $location = get_image_location($original_file);
            $latitude = $location['latitude'];
            $longitude = $location['longitude'];
            
            $uploadFolder = 'pictures' . DIRECTORY_SEPARATOR . 'full';
            $tumbnailFolder = 'pictures' . DIRECTORY_SEPARATOR . 'thumbnail';

            $originalFile =  $this->dir . DIRECTORY_SEPARATOR . $uploadFolder . DIRECTORY_SEPARATOR . $uuid . $type;
            $thumpnailFile = $this->dir . DIRECTORY_SEPARATOR . $tumbnailFolder . DIRECTORY_SEPARATOR . $uuid . $type;

            move_uploaded_file($original_file, $originalFile); //move file

            $this->createThumbnail($originalFile, $thumpnailFile, 200); //creates small picture

            $this->addValidTagsImage($tags, $uuid); //adds tags
			//upload picture into db
            $query = "INSERT INTO `images` (`user`, `id`, `original_name`, `title`, `description`, `image_type`, `original_file_reference`, `thumbnail_file_reference`, `latitude`, `longitude`, `imagesize`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $statement = $this->connection->prepare($query);
            if(!$statement){
                return false;
            }
            $statement->bind_param("ssssssssssd", $user, $uuid, $original_filename, $title, $description, $type, $originalFile, $thumpnailFile, $latitude,$longitude, $size);
            $statement->execute();

            $tagSelectQuery = "SELECT id FROM `tags` WHERE `title` = ?";

            return true;
        }
    }

    public function getImages($filterParams) { //get all images
        if($this->connection)
        {
            $query = "SELECT * FROM `images`";

            if(count($filterParams) > 0) {
                $query = $query . " WHERE " . implode(" AND ", $filterParams) . ";";
            } else {
                $query = $query . ";";
            }
            $statement = $this->connection->prepare($query);
            if(!$statement){
                die($this->connection->error);
            }
            $statement->execute();
        }
        return $statement->get_result()->fetch_all();
    }

    private function checkDbForUid($uid) //check DB for UID
    {
        if($this->connection)
        {
            $statement = $this->connection->prepare("SELECT * FROM `images` WHERE `id` = ?;");
            $statement->bind_param("s", $uid);
            $statement->execute();
        }  
        return $statement->get_result();
    }

    private function guidv4($data) //create random string
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    
    private function createUid() //create random uid
    {
        $uid = $this->guidv4(openssl_random_pseudo_bytes(16));
        $resultSet = $this->checkDbForUid($uid);
        
        if($resultSet->num_rows > 0)
        {
            return $this->createUid();
        }
        else 
        {
            return $uid;
        }
    }

    private function closeConnection() //close connection
    {
        $this->connection->close();
    }
    
    public function realEscapeString($string) //Sql safety measure
    {
        return $this->connection->real_escape_string($string);
    }
    
    public function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) //create small picute
    {
        $type = exif_imagetype($src);

        if (!$type || ($type != IMAGETYPE_JPEG && $type != IMAGETYPE_PNG)) 
        {
            return null;
        }

        if ($type === IMAGETYPE_JPEG)
        {
            $image = imagecreatefromjpeg($src);
        }
        else if ($type === IMAGETYPE_PNG)
        {
            $image = imagecreatefrompng($src);
        }

        if (!$image) 
        {
            return null;
        }

        $width = imagesx($image);
        $height = imagesy($image);

        if ($targetHeight == null) 
        {
            $ratio = $width / $height;

            if ($width > $height) 
            {
                $targetHeight = floor($targetWidth / $ratio);
            }
            else 
            {
                $targetHeight = $targetWidth;
                $targetWidth = floor($targetWidth * $ratio);
            }
        }

        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

        if ($type === IMAGETYPE_PNG) 
        {
            imagecolortransparent($thumbnail, imagecolorallocate($thumbnail, 0, 0, 0));

            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
        }

        imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        
        if ($type === IMAGETYPE_JPEG)
        {
            return imagejpeg($thumbnail, $dest, 100);
        }
        else if ($type === IMAGETYPE_PNG) 
        {
            return imagepng($thumbnail, $dest, 0);
        }

    }
    
    private function checkDbForTag($tag) //check for tags
    {
        $statement = $this->connection->prepare("SELECT `id` FROM `tags` WHERE `title`=?");
        $statement->bind_param("s", $tag);
        $statement->execute();
        return $statement->get_result()->fetch_array();
    }
    
    private function addTag($tagid, $uuid) //really adds tag
    {
        $query = "INSERT INTO `tag_list` (`tag_id`, `image_id`) VALUES (?, ?);";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ds", $tagid, $uuid);
        $statement->execute();
    }

    private function addValidTagsImage($tagsInput, $uuid) //adds valid tags
    {
        $tags = explode("║",$tagsInput);

        if(!empty($tags))
        {   
            foreach($tags as $tag) 
            {
                $f = explode("╓", $tag)[0];
                $v = explode("╓", $tag)[1];

                $resultSetTag = $this->checkDbForTag($v);
                
                if($resultSetTag[0])
                {
                    $this->addTag($resultSetTag[0], $uuid);
                }          
            }
        }
    }
}

function get_image_location($image = '') //gets GPS data of picture
{
    $exif = exif_read_data($image, 0, true);
    if($exif && isset($exif['GPS']))
    {
        $GPSLatitudeRef = $exif['GPS']['GPSLatitudeRef'];
        $GPSLatitude    = $exif['GPS']['GPSLatitude'];
        $GPSLongitudeRef= $exif['GPS']['GPSLongitudeRef'];
        $GPSLongitude   = $exif['GPS']['GPSLongitude'];
        
        $lat_degrees = count($GPSLatitude) > 0 ? gps2Num($GPSLatitude[0]) : 0;
        $lat_minutes = count($GPSLatitude) > 1 ? gps2Num($GPSLatitude[1]) : 0;
        $lat_seconds = count($GPSLatitude) > 2 ? gps2Num($GPSLatitude[2]) : 0;
        
        $lon_degrees = count($GPSLongitude) > 0 ? gps2Num($GPSLongitude[0]) : 0;
        $lon_minutes = count($GPSLongitude) > 1 ? gps2Num($GPSLongitude[1]) : 0;
        $lon_seconds = count($GPSLongitude) > 2 ? gps2Num($GPSLongitude[2]) : 0;
        
        $lat_direction = ($GPSLatitudeRef == 'W' or $GPSLatitudeRef == 'S') ? -1 : 1;
        $lon_direction = ($GPSLongitudeRef == 'W' or $GPSLongitudeRef == 'S') ? -1 : 1;
        
        $latitude = $lat_direction * ($lat_degrees + ($lat_minutes / 60) + ($lat_seconds / (60*60)));
        $longitude = $lon_direction * ($lon_degrees + ($lon_minutes / 60) + ($lon_seconds / (60*60)));

        return array('latitude'=>$latitude, 'longitude'=>$longitude);
    }
    else
    {
        return;
    }
}

function gps2Num($coordPart)
{
    $parts = explode('/', $coordPart);
    if(count($parts) <= 0)
        return 0;
    if(count($parts) == 1)
        return $parts[0];
    return floatval($parts[0]) / floatval($parts[1]);
}