<?php
session_start();

require_once("model/Image.class.php");

$imagedb = new ImageDB(dirname( __FILE__ ));

if (!empty($_FILES['upload'])) //checking for inputs existence
{   
    $file = $_FILES['upload'];
	//checking for errors
    if(empty($_POST["title"])) {
        $_SESSION["message"] = ["Kein Title angegeben!"];
        $_SESSION["message-type"] = "alert-danger";

        header("Location: index.php");
        return;
    } else if(empty($_POST["description"])) {
        $_SESSION["message"] = ["Keine Description angegeben!"];
        $_SESSION["message-type"] = "alert-danger";

        header("Location: index.php");
        return;
    } else { //uploading Image to DB
        $imagedb->uploadImage($file, $imagedb->realEscapeString($_POST["title"]), $imagedb->realEscapeString($_POST["description"]), $_POST["tags"], $_SESSION["userid"]);
        
        $_SESSION["message"] = ["Bild wurde hochgeladen!"];
        $_SESSION["message-type"] = "alert-success";

        header("Location: index.php");
    }
} 
else //no file error
{
    $_SESSION["message"] = ["Kein File wurde angegeben!"];
    $_SESSION["message-type"] = "alert-danger";

    header("Location: index.php");
}
