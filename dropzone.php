<script>
    var createDelButton;

    <?php
    if (!empty($_SESSION["userid"])) {
        echo "createDelButton = true";
    } else {
        echo "createDelButton = false";
    }
    ?>
    
    /* old
    Dropzone.autoDiscover = false;

    var upload = new Dropzone("form#upload-form", {
        url: "/Friedwagner_UE3/upload.php",
        acceptedFiles: "image/png,image/jpg,image/jpeg"
    });

    upload.on('success', function() {
        $('#image-list').empty();
        $('#image-list').append('<span class="loading">Loading<i class="fa fa-spinner fa-spin ml-2"></i></span>');
        $.ajax({
                method: "POST",
                url: "images.php",
                data: {
                    filterParams: $("#filter").val()
                }
            })
            .done(function(res) {
                $('#image-list').empty();
                setImages(res);
            });
    }); */

    function setImages(res) {

        $('#images-count').removeClass("d-none").text("Gallery shows " + res["images"].length + " out of " + res["allImageCount"] + " Images");

        for (let image = 0; image < res["images"].length; image++) {
            if (createDelButton === true) {
                $('#image-list').append('<div class="grid-item"><a href="' + res["images"][image]["originalFile"] + '" data-lightbox="gallery" data-title="' + res["images"][image]["name"] + '">\n\
            <img class="grid-iamge" src="' + res["images"][image]["thumbnailFile"] + '" /></a><span class="delete-button" id="removeButton' + image + '" data-removeFile="' + res["images"][image]["name"] + '">\n\
            <i class="fa fa-times"></i></span></div>');
            } else {
                $('#image-list').append('<div class="grid-item"><a href="' + res["images"][image]["originalFile"] + '" data-lightbox="gallery" data-title="' + res["images"][image]["name"] + '">\n\
            <img class="grid-iamge" src="' + res["images"][image]["thumbnailFile"] + '" /></a><</div>');
            }
        }

        if (res["images"].length === 0) {
            $('#image-list').append('<span class="no-images">Leider wurden entweder keine Bilder hochgeladen oder dein Filter gibt 0 Ergebnisse zurück!');
        } else {
            $('.delete-button').click(function() {
                $('#image-list').empty();
                $('#image-list').append('<span class="loading">Loading<i class="fa fa-spinner fa-spin ml-2"></i></span>');
                $.ajax({
                        method: "POST",
                        url: "delete.php",
                        data: {
                            file: $(this)[0].dataset.removefile,
                            filterParams: $("#filter").val()
                        }
                    })
                    .done(function(res) {
                        $('#image-list').empty();
                        setImages(res);
                    });
            })
        }

        var msnry = new Masonry('.grid', {
            columnWidth: 210,
            itemSelector: '.grid-item',
            gutter: 20
        });
    }
</script>