<?php
    session_start();
    unset($_SESSION["userid"]); //deletes sessions
	unset($_SESSION["is_admin"]);
    
    
    $_SESSION["message"] = ["Erfolgreich ausgeloggt"]; //sets messages
    $_SESSION["message-type"] = "alert-success";
    
    header("Location: index.php");
?>