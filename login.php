<?php
    require_once("model/User.class.php");

    session_start();
	//checking of inputs existance
    if(!empty($_POST["username"]) && !empty($_POST["password"]))
    {
        $username = $_POST["username"];
        $password = $_POST["password"];
        
        if (!isset($db))
        {
            $db = new DB();
        }
        //deleting dangerous symbols
        $username = $db->realEscapeString($username);
        //checking information in DB
        $resultSetUser = $db->getLoginUserdata($username);
        if ($resultSetUser->num_rows === 0) //checking for username
        {
            $_SESSION["message"] = ["Username exisitiert nicht!"];
            $_SESSION["message-type"] = "alert-danger";
            header("Location: index.php");
        }
        else if ($resultSetUser->num_rows === 1)
        {
            $result = $resultSetUser->fetch_assoc(); //loading information
  
            if(password_verify($password, $result["passwort"]))
            {
				if ($result["is_admin"] != 2) //succssesfull login
				{
					$_SESSION["userid"] = $result["id"];
					$_SESSION["is_admin"] = $result["is_admin"];
					$_SESSION["message"] = ["Erfolgreich eingeloggt!"];
					$_SESSION["message-type"] = "alert-success";
					header("Location: index.php");
				}
                                else //locked account
                                {
                                    $_SESSION["message"]= ["Account gesperrt!"];
                                    $_SESSION["message-type"] = "alert-danger";
                                }
                                header("Location: index.php");
			}
            else //wrong password
            {
                $_SESSION["message"] = ["Flasches Passwort!"];
                $_SESSION["message-type"] = "alert-danger";
                header("Location: index.php");
            }
            header("Location: index.php");
        }
        else if ($resultSetUser->num_rows > 1)
        {
            die("Die Kartoffelarmee hat angegriffen, rette sich wer kann!");
        }
        unset($db);
    }
    else //incomplete input
    {
        $_SESSION["message"] = ["Eingaben unvollständig!"];
        $_SESSION["message-type"] = "alert-danger";
        header("Location: index.php");
    }
     

?>