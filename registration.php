<?php
    require_once("model/User.class.php");

    session_start();
    
    $errors = array();
	//checking of inputs existance
    if(!empty($_POST["email"]) && !empty($_POST["username"]) && !empty($_POST["firstname"]) && !empty($_POST["lastname"]) && !empty($_POST["password"]) && !empty($_POST["passwordConfirmation"]))
    {
        if (!isset($db))
        {
            $db = new DB();
        }
        //loading of inputs
        $email = $_POST["email"];
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $password = $_POST["password"];
        $passwordConfirmation = $_POST["passwordConfirmation"];
        
        $username = $db->realEscapeString($username);
        $firstname = $db->realEscapeString($firstname);
        $lastname = $db->realEscapeString($lastname);
        $email = $db->realEscapeString($email);
        //checking of inputs for correctnes
        if (strlen($email) > 64)
        {
            array_push($errors, "Email zu lang! (maximal 64 Zeichen)");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
        {
            array_push($errors, "Falsches email Format!");
        }
        
        $resultSetEmail = $db->checkDbForEmail($email);
        if($resultSetEmail[0])
        {
            array_push($errors, "Email existiert bereits!");
        }
        
        if(strlen($username) > 32)
        {
            array_push($errors, "Username zu lang! (maximal 32 Zeichen)");
        }
        
        $resultSetUser = $db->checkDbForUsername($username);
        if($resultSetUser[0])
        {
            array_push($errors, "Username existiert bereits!");
        }
        
        if(strlen($firstname) > 32)
        {
            array_push($errors, "Vorname zu lang! (maximal 32 Zeichen)");
        }
        if(strlen($lastname) > 32)
        {
            array_push($errors, "Nachname zu lang! (maximal 32 Zeichen)");
        }
        if(strlen($password) > 60)
        {
            array_push($errors, "Passwort zu lang! (maximal 60 Zeichen)");
        }
        if($password != $passwordConfirmation)
        {
            array_push($errors, "Die beiden Passwörter stimmen nicht überein!");
        }
        
        if(count($errors) > 0)
        {
            $_SESSION["message"] = $errors;
            $_SESSION["message-type"] = "alert-danger";
        }
        else //loading information into db
        {             
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            $uid = createUid($db);
            
            $resultSet = $db->registerUser($uid, $username, $password_hash, $firstname, $lastname, $email);
            
            if ($resultSet)
            {
                $_SESSION["message"] = ["Erfolgreich registriert! Bitte logge dich ein!"];
                $_SESSION["message-type"] = "alert-success";
            }
            else 
            {
                $_SESSION["message"] = ["Der Server wurde von Kartoffeln attackiert! Bitte fernhalten!"];
                $_SESSION["message-type"] = "alert-danger";
            }
        }
        unset($db);
        header("Location: index.php");
    }
    else 
    {
        $_SESSION["message"] = ["Eingaben unvollständig!"];
        $_SESSION["message-type"] = "alert-danger";
        header("Location: index.php");
    }

    function guidv4($data) //creation of random string
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    
    function createUid($db) //UID creation
    {
        $uid = guidv4(openssl_random_pseudo_bytes(16));
        $resultSet = $db->checkDbForUid($uid);
        
        if($resultSet->num_rows > 0)
        {
            return createUid($db);
        }
        else 
        {
            return $uid;
        }
    }
    
    //INSERT INTO `user` (`id`, `username`, `password`, `vorname`, `nachname`, `email`, `is_admin`) VALUES ('163f8d84-7da3-4752-80cf-0fae978e7bef', 'Orka', 'abc', 'David', 'Friedwagner', 'david.fw3004@gmail.com', '1');
    ?>



