    <?php
    $user = NULL;
    if (!empty($_SESSION["userid"])) {
        $user = checkId($_SESSION["userid"]);
    }
	require_once("./model/User.class.php");
	if (!isset($db))
        {
            $db = new DB();
        }
	$user = NULL;
        if(!empty($_SESSION["userid"]))
        {
            $user = checkId($_SESSION["userid"]);
        }
    ?>

    <!--Registrierung-->
    <div class="modal fade" id="registrationForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-dark">
                <div class="modal-header modal-dark-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrierung</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: white;">&times;</span>
                    </button>
                </div>
                <form method="post" action="registration.php">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control input-main" id="email" name="email" aria-describedby="emailHelp" placeholder="email">
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control  input-main" id="username" name="username" placeholder="username">
                        </div>
                        <div class="form-group">
                            <label for="firstname">Vorname</label>
                            <input type="text" class="form-control  input-main" id="firstname" name="firstname" placeholder="vorname">
                        </div>
                        <div class="form-group">
                            <label for="lastname">Nachname</label>
                            <input type="text" class="form-control input-main" id="lastname" name="lastname" placeholder="nachname">
                        </div>
                        <div class="form-group">
                            <label for="password">Passwort</label>
                            <input type="password" class="form-control input-main" id="password" name="password" placeholder="passwort">
                        </div>
                        <div class="form-group">
                            <label for="passwordConfirmation">Passwort bestätigen</label>
                            <input type="password" class="form-control input-main" id="passwordConfirmation" name="passwordConfirmation" placeholder="passwort">
                        </div>
                    </div>
                    <div class="modal-footer modal-dark-footer">
                        <button type="submit" class="btn btn-main">Registrieren</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Profil bearbeiten-->
    <?php if ($user != null) { ?>
        <div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-dark">
                    <div class="modal-header modal-dark-header">
                        <h5 class="modal-title" id="exampleModalLabel">Profil bearbeiten</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="editProfile.php">
                        <div class="modal-body">
						<?php echo "Username: ".$user['username']; ?>
                            <div class="form-group">
                                <label for="email">Neue Email</label>
                                <input type="email" class="form-control input-main" id="e_email" name="email" aria-describedby="emailHelp" placeholder=<?php echo $user['email']; ?>>
                            </div>
                            <div class="form-group">
                                <label for="firstname">Neuer Vorname</label>
                                <input type="text" class="form-control input-main" id="e_firstname" name="firstname" placeholder=<?php echo $user['vorname']; ?>>
                            </div>
                            <div class="form-group">
                                <label for="lastname">Neuer Nachname</label>
                                <input type="text" class="form-control input-main" id="e_lastname" name="lastname" placeholder=<?php echo $user['nachname']; ?>>
                            </div>
                            <div class="form-group">
                                <label for="password">Aktuelles Passwort</label>
                                <input type="password" class="form-control input-main" id="e_password" name="password" placeholder="passwort">
                            </div>
                            <!--<div class="form-group">
                                <label for="passwordConfirmation">Neues Passwort bestätigen</label>
                                <input type="password" class="form-control input-main" id="e_passwordConfirmation" name="passwordConfirmation" placeholder="passwort">
                            </div>-->
                        </div>
                        <div class="modal-footer modal-dark-footer">
                            <button type="button" class="btn btn-main" data-toggle='modal' data-target='#changePw'>Passwort ändern</button>
                        </div>
                        <div class="modal-footer modal-dark-footer">
                            <button type="submit" class="btn btn-main">Übernehmen</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <div class="modal fade" id="changePw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-dark">
                    <div class="modal-header modal-dark-header">
                        <h5 class="modal-title" id="exampleModalLabel">Passwort ändern</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="changePw.php">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="passwordNew">Neues Passwort</label>
                                <input type="password" class="form-control input-main" id="c_passwordNew" name="passwordNew" placeholder="neues passwort">
                            </div>
                            <div class="form-group">
                                <label for="passwordConfirmation">Neues Passwort bestätigen</label>
                                <input type="password" class="form-control input-main" id="c_passwordConfirmation" name="passwordConfirmation" placeholder="neues passwort">
                            </div>
                            <div class="form-group">
                                <label for="password">Aktuelles Passwort</label>
                                <input type="password" class="form-control input-main" id="c_password" name="password" placeholder="aktuelles passwort">
                            </div>
                        </div>
                        <div class="modal-footer modal-dark-footer">
                            <button type="submit" class="btn btn-main">Übernehmen</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
        <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-dark">
                    <div class="modal-header modal-dark-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bild hochladen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="uploadNew.php" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-6">
                                    <div class="uploader">
                                        <img id="uploadPreview" src="assets/img/uploadImage.png" />
                                    </div>
                                    <input id="upload" name="upload" type="file" style="display: none" />
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="image_title">Bildtitel</label>
                                        <input type="text" class="form-control input-main" id="image_title" name="title" aria-describedby="" placeholder="Titel...">
                                    </div>
                                    <div class="form-group">
                                        <label for="image_description">Beschreibung</label>
                                        <textarea class="form-control input-main" id="image_description" name="description" aria-describedby="" placeholder="Beschreibung"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="image_tags">Tags</label>
                                        <div>
                                            <input type="text" id="image_tags" name="tags" class="form-control test">
                                        </div>
                                        <script>
                                            let value_delimiterB = "╓";

                                            let filter_optionsB = [];

                                            let filter_groupsB = [];

                                            var filter = $('#image_tags').selectize({
                                                persist: false,
                                                plugins: ['remove_button'],
                                                createOnBlur: true,
                                                delimiter: "║",
                                                create: function(input, callback) {
                                                    callback({
                                                        title: input,
                                                        id: "tag" + value_delimiterB + input
                                                    });
                                                },
                                                valueField: 'id',
                                                labelField: 'title',
                                                onItemAdd: function() {
                                                    
                                                },
                                                onItemRemove: function() {
                                                    
                                                },
                                                placeholder: "Add Tags...",
                                                searchField: 'title',
                                                options: filter_optionsB,
                                                optgroups: filter_groupsB,
                                                optgroupField: 'class',
                                                render: {
                                                    item: function(item, escape) {
                                                        return '<div class="item ' + escape(item.class) + '" data-value="' + escape(item.id) + '" style="background-color: #' + ('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6) + '!important;">' + escape(item.title) + '</div>';
                                                    },
                                                    optgroup_header: function(data, escape) {
                                                        return '<div class="optgroup-header ' + escape(data.value) + '">' + escape(data.label) + ' <span class="optgroup-header-hint ' + escape(data.value) + '">' + escape(data.label_hint) + '</span></div>';
                                                    }
                                                }
                                            })[0].selectize;
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4 pt-3" style="border-top: 2px solid #d11013;">
                                <div class="col-12">
                                    <button class="btn-main-reversed btn" id="imageUpload" type="submit">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>