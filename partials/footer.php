<footer class="container-fluid bg-dark">
    <div class="container">
        <div class="row">
            <div class="space"></div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a href="index.php?page=partials/impressum.php"><h4>Impressum</h4></a>

            </div>
            <div class="space"></div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a href="index.php?page=partials/help.php"><h4>Hilfe</h4></a>
            </div>
        </div>
    </div>
</footer>