<?php
    function getClass($type){
        if($type == "alert-danger"){
            return "exclamation-triangle";
        } else {
            return "info-circle";
        }
    }

    if (isset($_SESSION["message"]))
    {
    ?>
    <div class="row justify-content-center mt-4">
        <div class="col-sm-12 col-md-8 col-xl-6">
            <div class="alert <?php echo $_SESSION["message-type"]?> alert-dismissible fade show" role="alert">
                <span style="float:left;margin-right: 10px;"><i class="fa fa-<?php echo getClass($_SESSION["message-type"]) ?>"></i></span>
                <ul class="noSymbols">
                <?php
                    foreach($_SESSION["message"] as $message) 
                    {
                ?>
                    <li><?php echo $message ?></li>
                <?php
                    }
                ?>
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" stlye="color: white;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php
    }
    ?>