<section class="impressumagb container" id="hilfe">
  <h3>Hilfseite</h3>
  <div class="container-fluid">
    <h3>Funktionsweise</h3>
    <p>Als User stehen einem folgende Möglichkeiten zur Verfügung:
      <ul>
        <li>Gallerie
          <p>In der Gallerie kann man als User alle hochgeladenen Bilder ansehen und hat die Möglichkeit diese auch in Originalgröße anzuzeigen.</p>
        </li>
        <li>Kartenansicht
          <p>Hier haben Sie die Möglichkeit eine alternative Ansicht der Gallerie aufzurufen, wo, falls ein Bild über entsprechende Informationen verfügt, dieses auf dem entsprechenden Ort auf der Weltkarte markiert wird und in Kleinansicht mit den Koordinaten angesehen werden kann.</p>
        </li>
        <li>Impressum
          <p>Im Impressum finden Sie weitere rechtliche Informationen sowie Informationen über das Unternehmen.</p>
        </li>
        <li>Hilfe
          <p>Auf der Hilfe-Seite, auf der Sie sich bereits befinden, werden alle Funktionen und Seiten der Webseite erklärt.</p>
        </li>
      </ul>
    
    <h3>Eingeloggter Normaler User</h3>
    <p> Als normaler eingeloggten User stehen einem folgende zusätzliche Funktionen zur Verfügung:</p>
      <ul>
        <li>Bildverwaltung
          <p>Man hat nun die Möglichkeit eigene Bilder hochzuladen, diese Bilder zu verwalten, fremde Bilder zu kaufen und gekaufte Bilder herunterzuladen.</p>
        </li>
      </ul>

    <h3>Eingeloggter Admin-User</h3>
    <p>Als Administrator Steht einem eine wichtige Seite zur Verfügung: Die User-Verwaltungs-Seite.</p>
    <p>Hier können bereits erstellte User anzusehen und diese auf Knopfdruck zu löschen. Ein Administrator hat jedoch keine Möglichkeit Bilder zu kaufen.</p>

  </div>
</section>
