<?php
//ADMIN SITE
if ($_SESSION["is_admin"]!=1)
{
	header("Location: index.php");
}
echo "<div id='adm'>";
if (!isset($db))
    {
        $db = new mysqli("localhost", "root", "", "images");
    }

$idarray = array();
$idnum = 0;
//Creation of table
echo "<table>";
echo "	<tr>";
echo "		<th>ID</th>";
echo "		<th>Username</th>";
//echo "		<th>Anrede</th>";
echo "		<th>Vorname</th>";
echo "		<th>Nachname</th>";
//echo "		<th>Adresse</th>";
echo "		<th>PLZ</th>";
//echo "		<th>Ort</th>";
echo "		<th>EMail</th>";
echo "		<th>Is_Admin</th>";
echo "	</tr>";

$sql="SELECT * FROM user";

$res=$db->query($sql);
//Loading db data into table
while(($row = $res->fetch_assoc()) !== null)
{	
	array_push($idarray,$row["id"]);

	echo "	<tr>";
	echo "		<td>{$row["id"]}</td>";
	echo "		<td>{$row["username"]}</td>";
	//echo "		<td>{$row["anrede"]}</td>";
	echo "		<td>{$row["vorname"]}</td>";
	echo "		<td>{$row["nachname"]}</td>";
	//echo "		<td>{$row["adresse"]}</td>";
	echo "		<td>{$row["plz"]}</td>";
	//echo "		<td>{$row["ort"]}</td>";
	echo "		<td>{$row["email"]}</td>";
	echo "		<td>{$row["is_admin"]}</td>";
	if ($row["is_admin"]!=1)
	{
		echo "		<td><a class='text-danger' href=\"./userdelete.php?id=".$idarray[$idnum]."\">DELETE</a></td>";
		if ($row["is_admin"]==0)
		{
			echo "		<td><a class='text-danger' href=\"./userlock.php?id=".$idarray[$idnum]."\">LOCK</a></td>";
		}
		else
		{
			echo "		<td><a class='text-danger' href=\"./userlock.php?id=".$idarray[$idnum]."\">UNLOCK</a></td>";
		}
	}
	else
	{
		echo "		<td><a class='btn btn-main'>ADMIN<a></td>";
		echo "		<td><a class='btn btn-main'>ADMIN<a></td>";
	}
	echo "	</tr>";
	
	$idnum++;
	
}
echo "</table>";
echo "</div>";
?>