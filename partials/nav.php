<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php?page=partials/gallery.php#"><img src="assets/img/logo.png" class="main-logo" /></a>
    <?php
        $user = NULL;
        if(!empty($_SESSION["userid"]))
        {
            $user = checkId($_SESSION["userid"]);
        }
        
        if($user === NULL)
        {    
    ?>
    <form class="form-inline" id="filter-parent"><input type="text" id="filter" class="form-control"></form>
    
    <form class="form-inline" action="login.php" method="post">
        <input class ="form-control input-main" type="text" name="username" id="username" placeholder="username">
        <input class ="form-control input-main" type="password" name="password" id="password" placeholder="password">
        <button class="btn btn-main" type="submit">Login</button>
        <span id="registerBtn" class="btn btn-main" data-toggle="modal" data-target="#registrationForm">Registrieren</span>
    </form>

    <?php
        }
        else
        {   
        ?>
            
            <form class="form-inline" id="filter-parent">
                <input type="text" id="filter" class="form-control">
            </form>
            <form class="form-inline">
                
                <span class="user"><i class="fa fa-user fa-fw mr-1"></i><?php echo $user['vorname'] . " " . $user['nachname'] ?></span>
                <span class="btn btn-main mr-2" data-toggle='modal' data-target='#uploadImage' style="float: right">Bild hochladen</span>
                <span class='btn btn-main' data-toggle='modal' data-target='#editProfile'>Profil bearbeiten</span>
		<?php
                    if ($_SESSION["is_admin"]==1)	{ ?>
                    <a class='btn btn-main' href='index.php?page=partials/useradmin.php'>ADMIN_SITE</a>
                <?php } ?>
                    <a class='btn btn-main' href='logout.php'>Logout</a>
            </form>
            
            <?php
        }
    ?>
    
    
</nav>

