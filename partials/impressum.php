<div class="impressumagb">
	<h3 id="uschriftimpressum">Offenlegungspflicht gemäß § 25 Mediengesetz und Informationspflicht gemäß § 5 E-Commerce-Gesetz:</h3>
	<h3>Medieninhaber und Herausgeber</h3>
	<p>Verein Fachhochschule Technikum Wien<br />
	ZVR-Zahl 074476426</p>
	<p>Geschäftsführung</p>

	<p>Mariahilfer Straße 37-39 <br />
	1060 Wien <br />
	T: +43 1 588 39-0 <br />
	F: +43 1 588 39-49</p>

	<p>E-Mail: <a href="mailto:info@technikum-wien.at">info@technikum-wien.at</a></p>

	<h3>Geschäftsführung</h3>

	<p>Gabriele Költringer, EMBA (Geschäftsführerin), Ing. Dr. Michael Würdinger (Geschäftsführer)</p>

	<h3>Präsidium</h3>

	<ul><li>Dr. Lothar Roitner, Präsident der FH Technikum Wien und Geschäftsführer Fachverband der Elektro- und Elektronikindustrie (FEEI)</li>
		<li>Dr. Ulrike Baumgartner-Gabitzer, Austrian Power Grid AG</li>
		<li>Mag. Wilhelm Großeibl, Schrack Technik Gmbh </li>
		<li>DI Dr. Kurt Hofstädter, MBA (stv. Präsident), Siemens AG Österreich</li>
		<li>Dr. Kari Kapsch, Kapsch CarrierCom AG</li>
		<li>DI Anton Plimon, AIT Austrian Institute of Technology</li>
		<li>Mag. Dr. Gerhard Riemer, Industriellenvereinigung</li>
	</ul>
	<h3>Vereinszweck</h3>

	<p>1. Der Verein ist gemeinnützig und nicht auf Gewinn gerichtet und bezweckt die Förderung der Wissenschaft in Lehre und Forschung.<br /><br />
	2. Der Verein widmet sich als Erhalter von Fachhochschul-Studiengängen (§ 2 FHStG) der Errichtung, Erhaltung und dem Betrieb sowie der Weiterentwicklung von Fachhochschul-Studiengängen. Darüber hinaus soll das Bewusstsein für lebenslanges Lernen sowie auch die Akzeptanz des Fachhochschulwesens im allgemeinen in der Öffentlichkeit gefördert werden.</p>

	<h3>Tätigkeitsbereich</h3>

	<p>Die FH Technikum Wien bietet technische Studiengänge in Vollzeit- und/oder berufsbegleitender Form an. </p>

	<p>Ziel der FH Technikum Wien ist es, hoch qualifizierte TechnikerInnen auszubilden.</p>

	<h3>Grundlegende Richtung</h3>

	<p>Internetauftritt zur Förderung und Unterstützung des Tätigkeitsbereichs.</p>

	<h3>Für den Inhalt  verantwortlich</h3>

	<p>Abteilung Marketing und Kommunikation der FH Technikum Wien. Studiengangsbeschreibungen in Zusammenarbeit mit <a href="http://www.prazak.at" class="elf-external elf-icon" target="_blank">Content Agentur Prazak</a>.</p>

	<p>Fragen, Kommentare und Anregungen zur Website bitte an: <a href="mailto:web@technikum-wien.at" class="elf-mailto elf-icon">E-Mail</a></p>

	<h3>Haftungsausschluss</h3>

	<p>Das Team der FH Technikum Wien ist bemüht, sämtliche auf <a href="http://www.technikum-wien.at" class="elf-external elf-icon" target="_blank">www.technikum-wien.at</a> dargestellte Informationen inhaltlich richtig zu gestalten und laufend aktuell zu halten. Dessen ungeachtet übernimmt die FH Technikum Wien keine Garantie für Richtigkeit, Vollständigkeit und Aktualität der präsentierten Inhalte. Eine Haftung der FH Technikum Wien wird daher ausgeschlossen. Alle Links zu externen Seiten sind sorgfältig ausgewählt. Da die FH Technikum Wien auf deren Inhalt keinen Einfluss hat, übernimmt die FH Technikum Wien dafür keine Verantwortung.</p>

	<h3>Copyright</h3>

	<p>Das Layout der Website, die verwendeten Grafiken und Bilder, die Sammlung sowie einzelne Beiträge sind urheberrechtlich geschützt. Alle Rechte, auch die der fotomechanischen Wiedergabe, der Vervielfältigung und der Verbreitung mittels besonderer Verfahren - zum Beispiel Datenverarbeitung, Datenträger und Datennetze - auch teilweise, behält sich die FH Technikum Wien vor. </p>

	<h3>Datenschutzhinweis Google Analytics</h3>

	<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden</p>

	<h3>Veröffentlichung der Bildmarke gemäß § 19 Abs. 3 E-GovG</h3>

	<p>Im Sinne einer elektronischen Verfahrensabwicklung kann die Fachhochschule Technikum Wien auf ihren Dokumenten eine Amtssignatur aufbringen. Dadurch wird erkennbar, dass es sich um ein offizielles Dokument der FH Technikum Wien handelt. Durch die Amtssignatur können somit die Herkunft und die Unverfälschtheit eines Dokuments überprüft werden.</p>

	<p>Die Veröffentlichung der Bildmarke der Amtssignatur der FH Technikum Wien gemäß § 19 Abs. 3 E-Government-Gesetz (E-GovG) finden Sie <a href="https://www.technikum-wien.at/sites/default/files/amtssignatur-veroeffentlichung_fh_technikum_wien.pdf"><strong>hier</strong></a>.</p>

	<p><strong>Elektronische Signaturprüfung:</strong><br /><a href="http://www.signaturpruefung.gv.at" class="elf-external elf-icon" target="_blank">http://www.signaturpruefung.gv.at</a> bzw. <a href="https://pruefung.signatur.rtr.at/" class="elf-external elf-icon" target="_blank">https://pruefung.signatur.rtr.at/</a></p>

	<p><strong>Rückführbarkeit eines Ausdruckes mit Amtssignatur:</strong></p>
	<p>Sendung eines Scans an <a href="mailto:support@technikum-wien.at">support@technikum-wien.at</a> bzw. eine Kopie per Post.</p>
        
        <br>
        
        <p>Administratoren und Ersteller dieser Seite:</p>
        
        <div class="row">
            <div class="memberbox col-sm-6 col-md-6 col-lg-6">
                <h3>David Friedwagner</h3>
                <img src="assets/img/FriedwagnerDavid.jpg" alt="Bild von David">
            </div>

            <div class="memberbox col-sm-6 col-md-6 col-lg-6">
                <h3>Klemens Hamburger</h3>
                <img src="assets/img/HamburgerKlemens.jpg" alt="Bild von Klemnens">
            </div>
        </div>
</div>