<div class="container-fluid" id="gallery">
    <div class="row justify-content-center mt-2">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <a id="viewToggle" class='btn btn-main bg-dark' href='index.php?page=partials/map.php'>Kartenansicht</a>
                    <p id="images-count" class="d-none"><i class="fa fa-info-circle mr-1"></i></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="image-list" class="grid">
                        <span class="loading">Loading<i class="fa fa-spinner fa-spin ml-2"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
