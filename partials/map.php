<div class="container-fluid" id="gallery">
    <div class="row justify-content-center">
        <div class="col-sm-12" style="padding: 0px">
            <div id="mapid"></div>
        </div>
    </div>
</div>

<script>
let value_delimiter = "╓";

let filter_options = [{"id":"filetype╓png","class":"filetype","title":"File extension *.PNG"},
    {"id":"filetype╓jpg","class":"filetype","title":"File extension *.JPG"},
    {"id":"size╓1000","class":"size","title":"Larger then 1KB"},
    {"id":"size╓100000","class":"size","title":"Larger then 100KB"},
    {"id":"size╓5000000","class":"size","title":"Larger then 5MB"}];

let filter_groups = [{"value":"filetype","label":"File extension","label_hint":"Shows only the files with the selected file extension"},
    {"value":"size","label":"File Size","label_hint":"Shows file that have a size larger than selected!"}];

var filter = $('#filter').selectize(
    {
    persist: false,
    plugins: ['remove_button'],
    createOnBlur: true,
    delimiter: "║",
    create: function (input, callback)      
    {
        callback(
        {           
            title: "contains \"" + input + "\"",
            id: "fulltext" + value_delimiter + input
        });
    },
    valueField: 'id',
    labelField: 'title',
    onItemAdd: function() 
    {
        $.ajax(
        {
            method: "POST",
            url: "imagesNew.php",
            data: { filterParams: this.getValue() }
        })
        .done(function( res ) 
        {
            
        });
    },
    onItemRemove: function() 
    {
        $.ajax(
        {
            method: "POST",
            url: "imagesNew.php",
            data: { filterParams: this.getValue() }
        })
        .done(function( res ) 
        {
            
        });
    },
    placeholder: "Filter ...",
    searchField: 'title',
    options: filter_options,
    optgroups: filter_groups,
    optgroupField: 'class',
    render: 
    {
        item: function(item, escape) 
        {
            return '<div class="item ' + escape(item.class) + '" data-value="' + escape(item.id) + '">' + escape(item.title) + '</div>';
        },
        optgroup_header: function(data, escape) 
        {
            return '<div class="optgroup-header ' + escape(data.value) + '">' + escape(data.label) + ' <span class="optgroup-header-hint ' + escape(data.value) + '">' + escape(data.label_hint) + '</span></div>';
        }
    }
    })[0].selectize;

    $.ajax(
        {
            method: "POST",
            url: "imagesNew.php",
            data: { filterParams: $('#filter').val()}
        })
        .done(function( res ) 
        {
            $('#image-list').empty();
            setImagesToMap(res);
        });
</script>

<script>
    var mymap = L.map('mapid').setView([48.239264,16.377302], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', 
    {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/dark-v10',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoieGQwbTMiLCJhIjoiY2tiaWJudm91MGR1ejJ0cXZ5ZDZpcDR5MiJ9.hWlNZiGxvBr9EHfdMLHWkA'
    }).addTo(mymap);

    var marker = L.marker([48.239264,16.377302]).addTo(mymap);
    marker.bindPopup("<img src='assets/img/fhlogo.png' style='width: 100%;'></img>");
    
    function setImagesToMap(res) 
    {
        for (let image = 0; image < res["images"].length; image++) 
        {
            if(res["images"][image]["latitude"] !== "" && res["images"][image]["longitude"] !== "")
            {
                var marker = L.marker([res["images"][image]["latitude"],res["images"][image]["longitude"]]).addTo(mymap);
                marker.bindPopup('<img src="' + res["images"][image]["originalFile"] + '" style="width: 100%;"></img><br>' + res["images"][image]["latitude"] + ', ' + res["images"][image]["longitude"]);
            }
        }
    }
</script>